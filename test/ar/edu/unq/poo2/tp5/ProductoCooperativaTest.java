package ar.edu.unq.poo2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductoCooperativaTest {
	Producto producoop;
	
	@BeforeEach
	void setUp() {
		producoop = new ProductoCooperativa(100d,5);
	}
	
	@Test
	void testPrecio() {
		assertEquals(90d, producoop.getMonto());
	}

}
