package ar.edu.unq.poo2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ProductoTradicionalTest {
	Producto productradi;
	
	@BeforeEach
	void setUp() {
		productradi = new ProductoTradicional(75d, 8);
	}
	
	@Test
	void testStock() {
	    assertEquals(8, productradi.getStock());
	    productradi.decrementarStock();
	    assertEquals(7, productradi.getStock());
	}
	
	@Test
	void testPrecio() {
		assertEquals(75d, productradi.getMonto());
	}

}
