package ar.edu.unq.poo2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class CajaTest {
	Caja caja;
	AgenciaDePrueba agencia;
	
	@BeforeEach
	void setUp() {
		agencia = new AgenciaDePrueba();
		caja = new Caja();
		caja.add(new ProductoCooperativa(100d,5));
		caja.add(new ProductoTradicional(100d,8));
		caja.add(new ProductoCooperativa(50d,7));
		caja.add(new FacturaDeImpuestos(agencia, 21d));
		caja.add(new FacturaDeServicios(agencia, 21d, 10d));
		
	}
	
	@Test
	void testMontoTotal() {
		assertEquals(466d, caja.getMontoTotal());
	}
	
	@Test
	void testCobrar() {
		caja.cobrar();
		assertEquals(231d, agencia.getRecaudacion());
	}

}
