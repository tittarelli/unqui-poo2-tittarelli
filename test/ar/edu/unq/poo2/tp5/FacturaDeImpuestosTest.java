package ar.edu.unq.poo2.tp5;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FacturaDeImpuestosTest {
	Factura factax;
	AgenciaDePrueba agencia;
	
	@BeforeEach
	void setUp() {
		agencia = new AgenciaDePrueba();
		factax = new FacturaDeImpuestos(agencia,21d);
	}
	
	@Test
	void agenciaTest() {
		factax.cobrarse();
		assertEquals(agencia.getRecaudacion(), 21d);
	}

}
