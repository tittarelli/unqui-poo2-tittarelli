package unq;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.poo2.tp4.Producto;
import ar.edu.unq.poo2.tp4.Supermercado;

import static org.junit.jupiter.api.Assertions.*;

public class SupermercadoTest {
	
	private Producto arroz;
	private Producto detergente;
	private Supermercado supermercado;
	
	@BeforeEach
	public void setUp() {
		arroz = new Producto("Arroz", 18.9d, true);
		detergente = new Producto("Detergente", 75d);
		supermercado = new Supermercado("Lo de Tito", "Av Zubeldia 801");
		
	}
	
	@Test
	public void testCantidadDeProductos() {
		assertEquals(0, supermercado.getProductsAmount());
		supermercado.addProduct(arroz);
		supermercado.addProduct(detergente);
		assertEquals(2, supermercado.getProductsAmount());
	}
	
	@Test
	public void testPrecioTotal() {
		assertEquals(0d, supermercado.getTotalPrice());
		supermercado.addProduct(arroz);
		supermercado.addProduct(detergente);
		assertEquals(93.9d, supermercado.getTotalPrice());
	}
}
