package unq;
import ar.edu.unq.poo2.tp3.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class MultioperadorTestCase {    
    private Multioperador multioperador;    
    
    /**
     * Crea un escenario de test b�sico, que consiste en un contador 
     * con 10 enteros
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
   	 
   	 //Se crea el contador
   	 multioperador = new Multioperador();
   	 
   	 //Se agregan los numeros. Un solo par y nueve impares
   	multioperador.addNumber(20);
   	multioperador.addNumber(3);
   	multioperador.addNumber(5);
    }
 
    /**
     * Verifica la cantidad de pares
     */
    @Test
    public void testSumatoria() {
   	 
   	 // Getting the even occurrences
   		 int amount = multioperador.sumarTodos();
   			 
   	 // I check the amount is the expected one
   		assertEquals(amount, 28);
    }
    
    @Test
    public void testResta() {
    	int amount = multioperador.restarTodos();
    	assertEquals(amount, 12);
    }
    
    @Test
    public void testMultiplicacion() {
    	int amount = multioperador.multiplicarTodos();
		assertEquals(amount, 300);
    }
    
}

