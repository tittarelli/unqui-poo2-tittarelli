package unq;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import ar.edu.unq.poo2.tp4.Producto;

import static org.junit.jupiter.api.Assertions.*;

public class ProductoTest {
	
	private Producto arroz;
	private Producto vino;
	
	@BeforeEach
	public void setUp() {
		arroz = new Producto("Arroz", 18.9d, true);
		vino = new Producto("Vino", 55d);
	}
	
	@Test
	public void testConstructor() {
		assertEquals("Arroz", arroz.getName());
		assertEquals(18.9d, arroz.getPrice());
		assertTrue(arroz.isPreciosCuidados());
		
		assertEquals("Vino", vino.getName());
		assertEquals(55d, vino.getPrice());
		assertFalse(vino.isPreciosCuidados());
	}
	
	@Test
	public void testAumentarPrecio() {
		arroz.increasePrice(1.5);
		assertEquals(20.4d, arroz.getPrice());
	}
}
