package unq;
import ar.edu.unq.poo2.tp4.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class IngresoTest {

	private Ingreso ingreso;
	
	@BeforeEach
	public void setUp() {
		ingreso = new Ingreso("Enero", "Por sus servicios brindados", 2000);
	}
	
	@Test
	public void testIngreso() {
		assertEquals(ingreso.getMontoImponible(), 2000d);
	}
	
	
}
