package unq;
import ar.edu.unq.poo2.tp3.*;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;
 
public class CounterTestCase {    
    private Counter counter;    
    
    /**
     * Crea un escenario de test b�sico, que consiste en un contador 
     * con 10 enteros
     * 
     * @throws Exception
     */
    @Before
    public void setUp() throws Exception {
   	 
   	 //Se crea el contador
   	 counter = new Counter();
   	 
   	 //Se agregan los numeros. Un solo par y nueve impares
   	 counter.addNumber(1);
   	 counter.addNumber(3);
   	 counter.addNumber(5);
   	 counter.addNumber(7);
   	 counter.addNumber(9);
   	 counter.addNumber(1);
   	 counter.addNumber(1);
   	 counter.addNumber(1);
   	 counter.addNumber(1);
   	 counter.addNumber(4);
    }
 
    /**
     * Verifica la cantidad de pares
     */
    @Test
    public void testEvenNumbers() {
   	 
   	 // Getting the even occurrences
   		 int amount = counter.cantidadDePares();
   			 
   	 // I check the amount is the expected one
   		 assertEquals(amount, 1);
    }
    
    @Test
    public void testNumerosImpares() {
    	
    	int cantidad = counter.cantidadDeImpares();
    	
    	assertEquals(cantidad, 9);
    	
    }
    
    @Test
    public void testMultiplos() {
    	
    	int cantidad = counter.cantidadDeMultiplosDe(3);
    	
    	assertEquals(cantidad, 2);
    }
 
}
