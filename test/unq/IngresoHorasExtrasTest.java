package unq;

import ar.edu.unq.poo2.tp4.*;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class IngresoHorasExtrasTest {

	private IngresoPorHorasExtra ingresoporhhe;
	private Ingreso ingreso;
	
	@BeforeEach
	public void setUp() {
		ingresoporhhe = new IngresoPorHorasExtra("Febrero", "Horitas", 100, 8);
		ingreso = new Ingreso("Febrero", "Ingreso", 300);
	}
	
	@Test
	public void testIngresos() {
		assertEquals(ingresoporhhe.getMontoImponible(), 0);
		assertEquals(ingresoporhhe.getMonto(), 100);
		assertEquals(ingreso.getMontoImponible(), 300);
	}

}
