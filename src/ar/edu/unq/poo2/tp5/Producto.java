package ar.edu.unq.poo2.tp5;

public abstract class Producto implements Cobrable {
	private int stock;
	private double precio;
	
	public Producto(double precio, int stock) {
		this.precio=precio;
		this.stock=stock;
	}
	
	public void cobrarse() {
		this.decrementarStock();
	}

	public int getStock() {
		return this.stock;
	}

	public void decrementarStock() {
		stock--;
	}

	public double getMonto() {
		return this.precio;
	}

}
