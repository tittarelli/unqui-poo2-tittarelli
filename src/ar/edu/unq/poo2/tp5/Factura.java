package ar.edu.unq.poo2.tp5;

public abstract class Factura implements Cobrable {
	private Agencia agencia;
	
	public Factura(Agencia agencia) {
		this.agencia = agencia;
	}
	
	public void cobrarse() {
		this.agencia.registrarPago(this);
	}
	
	public abstract double getMonto();
}
