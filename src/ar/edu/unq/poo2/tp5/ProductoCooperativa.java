package ar.edu.unq.poo2.tp5;

public class ProductoCooperativa extends Producto{

	public ProductoCooperativa(double precio, int stock) {
		super(precio, stock);
	}
	
	@Override
	public double getMonto(){
		double precioAnterior = super.getMonto();
		return precioAnterior - (precioAnterior * 0.1);
	}

}
