package ar.edu.unq.poo2.tp5;

public class FacturaDeImpuestos extends Factura {

	private double tasa;

	public FacturaDeImpuestos(Agencia agencia, double tasa) {
		super(agencia);
		this.tasa = tasa;
	}

	@Override
	public double getMonto() {
		return this.tasa;
	}

}
