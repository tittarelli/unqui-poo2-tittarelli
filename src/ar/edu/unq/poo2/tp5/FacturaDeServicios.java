package ar.edu.unq.poo2.tp5;

public class FacturaDeServicios extends Factura {

	private double unidadesConsumidas;
	private double costoPorUC;

	public FacturaDeServicios(Agencia agencia, double unidadesConsumidas, double costoPorUC) {
		super(agencia);
		this.unidadesConsumidas = unidadesConsumidas;
		this.costoPorUC = costoPorUC;
	}

	@Override
	public double getMonto() {
		return this.unidadesConsumidas * this.costoPorUC;
	}

}
