package ar.edu.unq.poo2.tp5;
import java.util.ArrayList;
import java.util.Collection;
public class Caja {

	private Collection<Cobrable> cobrables = new ArrayList<>();

	public void add(Cobrable cobrable) {
		cobrables.add(cobrable);
	}

	public double getMontoTotal() {
		return cobrables.stream().mapToDouble(cobrable -> cobrable.getMonto()).sum();
	}
	
	public void cobrar() {
		cobrables.forEach(cobrable -> cobrable.cobrarse());
	}

}
