package ar.edu.unq.poo2.tp5;

public class AgenciaDePrueba implements Agencia{

	private double recaudacion;

	@Override
	public void registrarPago(Factura factura) {
		this.recaudacion += factura.getMonto();
	}

	public double getRecaudacion() {
		return recaudacion;
	}
	
}