package ar.edu.unq.poo2.tp5;

public interface Cobrable {
	public double getMonto();
	public void cobrarse();
}
