package ar.edu.unq.poo2.tp3;

import java.util.ArrayList;

public class Multioperador {
  ArrayList<Integer> listaDeNumeros = new ArrayList<>();
  int suma = 0;
  int resta;
  int multiplicacion = 1;
  
  public void addNumber(int num) {
	  listaDeNumeros.add(num);
  }
  
  public int sumarTodos() {
	  listaDeNumeros.stream().forEach(numero -> suma+=numero);
	  return suma; 
  }
  
  public int restarTodos() {
	  resta = listaDeNumeros.get(0);
	  listaDeNumeros.stream().forEach(numero -> resta-=numero);
	  return resta +(listaDeNumeros.get(0));
  }
  
  public int multiplicarTodos() {
	  listaDeNumeros.stream().forEach(numero -> multiplicacion=multiplicacion*numero);
	  return multiplicacion;
  }
}
