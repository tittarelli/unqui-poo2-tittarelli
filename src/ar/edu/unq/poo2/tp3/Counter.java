package ar.edu.unq.poo2.tp3;

import java.util.ArrayList;

public class Counter {
	ArrayList<Integer> listaDeNumeros = new ArrayList<>();
	
	public void addNumber(int num) {
		listaDeNumeros.add(num);
	}
	
	public int cantidadDePares() {
		return (int)listaDeNumeros.stream().filter(numero -> this.esPar(numero)).count();
	}
	
	public int cantidadDeImpares() {
		return (int)listaDeNumeros.stream().filter(numero -> !this.esPar(numero)).count();
	}
	
	public int cantidadDeMultiplosDe(int num) {
		return (int)listaDeNumeros.stream().filter(numero -> this.esMultiploDe(numero, num)).count();
	}
	
	public boolean esMultiploDe(int a, int b) {
		return (a % b) == 0;
	}
	
	public boolean esPar(int num) {
		return (num % 2) == 0;
	}
}
