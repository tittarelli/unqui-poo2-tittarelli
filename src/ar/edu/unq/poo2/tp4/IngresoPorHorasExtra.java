package ar.edu.unq.poo2.tp4;

public class IngresoPorHorasExtra extends Ingreso{
	
	private int cantidadDeHoras;
	
	public IngresoPorHorasExtra(String mes, String concepto, double monto, int horas) {
		super(mes, concepto, monto);
		setCantidadDeHoras(horas);
	}

	private void setCantidadDeHoras(int horas) {
		cantidadDeHoras = horas;
	}
	
	public int getCantidadDeHoras() {
		return cantidadDeHoras;
	}
	
	@Override
	public double getMontoImponible() {
		return 0;
	}

}
