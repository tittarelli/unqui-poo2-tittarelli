package ar.edu.unq.poo2.tp4;

import java.util.ArrayList;
import java.util.stream.DoubleStream;

public class Supermercado {

	private String name;
	
	private String Adress;
	
	private ArrayList<Producto> productos = new ArrayList<>();

	public Supermercado(String name, String adress) {
		setName(name);
		setAdress(adress);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAdress() {
		return Adress;
	}

	public void setAdress(String adress) {
		Adress = adress;
	}

	public int getProductsAmount() {
		return (int)productos.stream().count();
	}

	public void addProduct(Producto product) {
		productos.add(product);
	}

	public Double getTotalPrice() {
		return this.priceList().sum();
	}

	private DoubleStream priceList() {
		return productos.stream().mapToDouble(product -> product.getPrice());
	}

}
