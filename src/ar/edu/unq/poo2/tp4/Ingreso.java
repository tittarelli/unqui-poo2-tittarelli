package ar.edu.unq.poo2.tp4;

public class Ingreso {
	private String mes;
	private String concepto;
	private double monto;
	
	public String getMes() {
		return mes;
	}

	public void setMes(String mes) {
		this.mes = mes;
	}

	public String getConcepto() {
		return concepto;
	}

	public void setConcepto(String concepto) {
		this.concepto = concepto;
	}

	public double getMonto() {
		return monto;
	}

	public void setMonto(double monto) {
		this.monto = monto;
	}

	public Ingreso(String mes, String concepto, double monto) {
		setMonto(monto);
		setConcepto(concepto);
		setMes(mes);
	}

	public double getMontoImponible() {
		return monto;
	}

}
