package ar.edu.unq.poo2.tp4;

public class ProductoPrimeraNecesidad extends Producto{
	
	private double discount;
	
	public ProductoPrimeraNecesidad(String name, double price) {
		super(name, price);
	}
	
	public ProductoPrimeraNecesidad(String name, double price, int discount, boolean value) {
		super(name, price, value);
		setDiscount(discount);
	}
	
	public void setDiscount(double discount) {
		this.discount = discount/100;
	}
	
	private double getDiscount() {
		return (1 - discount);
	}
	
	@Override
	public double getPrice() {
		return super.getPrice() * getDiscount();//- ((super.getPrice()*discount)/100);
	}
}
