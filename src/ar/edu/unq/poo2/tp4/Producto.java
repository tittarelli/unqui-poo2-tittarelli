package ar.edu.unq.poo2.tp4;

public class Producto {
	private double price;
	private String name;
	private boolean isPreciosCuidados = false;
	
	public Producto(String name, double price, boolean value) {
		setName(name);
		setPrice(price);
        makePreciosCuidados(value);
	}
	
	public Producto(String name, double price) {
		this.name = name;
		this.price = price;
	}
	
	public void increasePrice(double amount) {
		price+=amount;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void makePreciosCuidados(boolean value) {
		if(value) {
			this.isPreciosCuidados = true;
		}
	}
	
	public boolean isPreciosCuidados() {
		return isPreciosCuidados;
	}
}
